package dyn_bindung.animal;

public class Cat extends Animal {

	
	public Cat(String name) {
		super(name);
	}
	
	
    @Override
    public String getSpecies() {
        return "cat";
    }

    @Override
	public String getTypicalSound() {
        return "meeow";
    }
    
    
    @Override
    public boolean isFriendWith(Animal other) {
    	return false;
    }
    
    
    public boolean isFriendWith(Cat other) {
    	return true;
    }
    

}
