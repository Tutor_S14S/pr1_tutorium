package dyn_bindung.animal;

public class Dog extends Animal {

	public Dog(String name) {
		super(name);
	}
	
    @Override
    public String getSpecies() {
        return "dog";
    }

    @Override
    public String getTypicalSound() {
        return "woof";
    }
    
    
    @Override
    public boolean isFriendWith(Animal other) {
    	return false;
    }

}
