package dyn_bindung.animal;

public class Cow extends Animal {

	public Cow(String name) {
		super(name);
	}
	
    @Override
    public String getSpecies() {
        return "cow";
    }

    @Override
    public String getTypicalSound() {
        return "moooh";
    }
    
    
    @Override
    public boolean isFriendWith(Animal other) {
    	return false;
    }

}
