package dyn_bindung.animal;

public abstract class Animal {
	
	private String name;
	
	public Animal(String name) {
		this.name = name;
	}
    
    public abstract String getSpecies();
    
    public abstract String getTypicalSound();
    
    public String getName() {
    	return name;
    }
    
    public abstract boolean isFriendWith(Animal other);
    
}
