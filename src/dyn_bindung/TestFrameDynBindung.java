package dyn_bindung;

import dyn_bindung.animal.Animal;
import dyn_bindung.animal.Cat;
import dyn_bindung.animal.Cow;
import dyn_bindung.animal.Dog;


public class TestFrameDynBindung {

	
    public static void main(String[] args) {
        
    	Animal[] animals = new Animal[3];
        
        animals[0] = new Cat("Muschi");
        animals[1] = new Dog("Bello");
        animals[2] = new Cow("Elfriede");
        
        // Calls specific methods for each type of animal in array
        for(int i=0; i<animals.length; i++){
            System.out.println("The " + animals[i].getSpecies()
                          + " goes: " + animals[i].getTypicalSound() + "!");
            System.out.println("Name: " + animals[i].getName());
        }
        
    }


}
