package dyn_bindung;

import dyn_bindung.animal.Animal;
import dyn_bindung.animal.Cat;
import dyn_bindung.animal.Dog;


public class TestFrameOverloading {
	
	
    public static void main(String[] args) {
        
    	Cat cat1 = new Cat("Mieze");
    	Cat cat2 = new Cat("Peter");
    	Dog dog  = new Dog("Bello");
        Animal cat3 = new Cat("Muschi");
    	
    	System.out.println( cat1.isFriendWith(cat2) );		// calls isFriendWith(Cat other) - all right
    	System.out.println( cat1.isFriendWith(dog)  );		// calls isFriendWith(Animal other) - all right
    	System.out.println( cat1.isFriendWith(cat3) );		// calls isFriendWith(Animal other) - ### FAIL ###
    	
    }
    
    
}
